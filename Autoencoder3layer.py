from keras.layers import Input, Dense
from keras.models import Model
import keras.backend as K
from keras.callbacks import ModelCheckpoint
from keras.callbacks import CSVLogger
from keras import optimizers
import pandas as pd
import numpy as np
import tensorflow as tf
from keras.backend import tensorflow_backend as K

tp = pd.read_csv(filepath_or_buffer='path to the gene expression profile file',sep='\t',na_values='.',header=None,iterator=True, chunksize=1000,low_memory=False)
df = pd.concat(tp, ignore_index=True) 
bb1 = df.values
dd = bb1.astype('float32')
# Divide samples for training and validation set
s_tr = dd[:,range(0,20000)]
s_te = dd[:,range(20000,27887)]
# Normalising the data for autoencoder
ttxrmi = s_tr.min(axis=1)
ttxrma = s_tr.max(axis=1)
sstmima = ttxrma - ttxrmi
s_tr = (s_tr - ttxrmi[:,None])/sstmima[:,None]
s_te = (s_te - ttxrmi[:,None])/sstmima[:,None]
s_tr = np.transpose(s_tr)
s_te = np.transpose(s_te)
comp_dim = 512 # Number of hidden nodes in each the layers
nofgene = 20848 # Total number of genes in the profile
input_Sig = Input(shape=(nofgene,))
L1 = Dense(comp_dim, activation='sigmoid')(input_Sig)
L2 = Dense(comp_dim, activation='sigmoid')(L1)
L3 = Dense(comp_dim, activation='sigmoid')(L2)
Lf = Dense(nofgene, activation='sigmoid')(L3)
autoencoder = Model(input_Sig, Lf)
admO = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-8, decay=1e-6)
autoencoder.compile(optimizer=admO, loss='mean_squared_error')
new_filename1 = "MSE_deepAE" + str(comp_dim) + ".csv"
csv_logger = CSVLogger(new_filename1, append=True, separator='\t')
############################## Training #################################
autoencoder.fit(s_tr, s_tr, epochs=40000, batch_size=256, shuffle=True, validation_data=(s_te, s_te), callbacks=[csv_logger])
new_filename2 = "MicroarrayDeep512_512_512_AE20K.h5"
autoencoder.save(new_filename2)