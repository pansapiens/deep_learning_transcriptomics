from keras.layers import Input, Dense, Activation
import numpy as np
from keras.models import Model, load_model
from keras import optimizers, activations
import math
from numpy import *
import pandas as pd
from keras.callbacks import ModelCheckpoint
from keras.callbacks import CSVLogger
from keras import optimizers
import tensorflow as tf
from keras.backend import tensorflow_backend as K
import glob

yykt = glob.glob("Path to the folder having all the files of phenotipic associated microarray gene expression profile/*.txt")
yykt = sorted(yykt)
count=0
b = np.identity(len(yykt), dtype = float) 
for fl in yykt:
	if count==0:
		UXU = np.loadtxt(fl)
		yead1e = UXU.shape
		Trg0t = np.repeat(b[:,count], repeats = yead1e[1], axis=0).reshape(len(yykt),yead1e[1])
	else:
		VVV = np.loadtxt(fl)
		yead1e = VVV.shape
		Trgft = np.repeat(b[:,count], repeats = yead1e[1], axis=0).reshape(len(yykt),yead1e[1])
		Trg0t = np.append(Trg0t,Trgft, axis=1)
		UXU = np.append(UXU,VVV, axis=1)
	count = count+1

qfwqwcv = np.random.permutation(UXU.shape[1])
Trg0t = Trg0t[:,qfwqwcv]
UXU = UXU[:,qfwqwcv]
Etr = np.transpose(UXU)
Dtr = np.transpose(Trg0t)
comp_dim = 512
n_genes = 20848 #We can replace this number according to the number of genes in the expression 
inputS_Sig = Input(shape=(n_genes,))
L1 = Dense(comp_dim , activation='sigmoid',name='X1')(inputS_Sig)
L2 = Dense(len(yykt), activation='sigmoid',name='X2')(L1)
DeasMSDeepNN = Model(inputS_Sig, L2)
admO = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-8, decay=1e-5)
DeasMSDeepNN.compile(optimizer=admO, loss = 'binary_crossentropy') 
new_filename1 = "Supervised3L1" + str(comp_dim) + ".csv"
csv_logger = CSVLogger(new_filename1, append=False, separator='\t')
DeasMSDeepNN.fit(Etr,Dtr,  epochs=5000, batch_size=32, shuffle=True, callbacks=[csv_logger])

b = np.identity(20848, dtype = float) 
UUr = DeasMSDeepNN.predict(b)

ytre = np.genfromtxt('RefEntrezMicroarray.txt',dtype='str')
yytrv = np.argsort(-np.absolute(UUr), axis=0)
entNe = ytre[yytrv]
np.savetxt("DiseaseGeneSupervisedNaive.txt", entNe,delimiter='\t',fmt='%s')
np.savetxt("OrderDisease.txt", yykt,delimiter='\t',fmt='%s')
