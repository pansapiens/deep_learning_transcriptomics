(1) Autoencoder3layer.py generates 3 layer deep autoencoder(deepAE), it takes gene expression profile file as an input and gives the file
    MicroarrayDeep512_512_512_AE20K.h5 of the trained autoencoder. The autoencoders trained on the micro-array and RNA-seq data are avaliable at https://figshare.com/articles/Autoencoder_trained_on_transcriptomic_signals/9092045.
    MicroarrayDeep512_512_512_AE20K.h5 is compatible with keras >= 2.0.8 and tensorflow >= 1.3.0 under the Python-3.6.1, also we used these versions for training the autoencoders.
    If the following codes that use a trained autoencoder and are not compatible with the version of tesnorflow and keras, then the code Autoencoder3layer.py is needed to run on the
    installed version of tesnorflow and keras in the machine which has enough RAM for loading the transcriptomics data. We run the code Autoencoder3layer.py on Kebnekaise cluster
    at the High Performance Computing Center North (HPC2N), Umeå University (Sweden) by using NVIDIA Tesla K80 (Kepler) type of GPU CARD and CPU with 120 GB RAM. The codes take
    40000 and 30000 epochs for micro-array and RNA-seq data to saturate mean square error respectively.

(2) ReverseTrainFirstLayer.py generates prioritized disease genes from the first hidden layer representations of the 3 layer deepAE, it takes
    the path to the folder having all the files of normalized gene expression values between 0 and 1 for each gene
    as we normalized for training the autoencoder and each file is associated with a certain phenotype. This code also needs
    the trained autoencoder, MicroarrayDeep512_512_512_AE20K.h5, and the reference gene file, RefEntrezMicroarray.txt, as input and prioritize the genes
    in the output file DeepAE_DiseaseGene.txt for each phenotype in the columns and maintains the the association of the columns in a same order
    in which the gene expression files are saved in the OrderDisease.txt file. ReverseTrainSecondLayer.py and ReverseTrainThirdLayer.py, generate prioritized
    disease genes similarly from the second and third hidden layer representations of the 3 layer deepAE respectively. Next, ReverseTrain1HLayer.py define the disease genes based on the
    hidden layer of shallow autoencoder (ShallowAE), it takes MicroarrayShallow512AE20K.h5 as an input file for the ShallowAE and ShallowAE_DiseaseGene.txt have the list of priotised genes
    associated with the phynotypes of the samples saved in the OrderDisease.txt file. Also, the codes ReverseTrainFirstLayer_RnaSeq.py, ReverseTrainSecondLayer_RnaSeq.py
    and ReverseTrainThirdLayer_RnaSeq.py derive the disease modules from first, second and third layer of 3 layer deep autoencoder trained on RNA-seq data. 
    
(3) The code Supervised_Naive.py predicts disease gene based on supervised trained as naive approach in file DiseaseGeneSupervisedNaive.txt with the columns having same order associated with the phynotypes of the samples
    saved in the OrderDisease.txt file, orrange color bars in the Fig3 a and b of the manuscript correpond to this prediction. 
    
(4)  "SNPclosestGenes" and DiseaseOntologyGeneData folders contain files for the entrez ID of the genes closest to the disease associated SNP available on DisGeNET database and entrez ID of genes associated
     disease based on Disease Ontology, http://disease-ontology.org/, annotations respectively.

(5) "RefEntrezMicroarray.txt" and "RefEntrezRNAseq.txt" is a reference gene file of the micro-array and RNA-seq transcriptomic data, which is in the same order features
     in which we trained autoencoder.

(6) Lightup_FromFirstLayer.py prioritize the genes to inferring with the PPI module association from first hidden layer of the 3 layer deepAE, this code takes
    the trained autoencoder, e.g. MicroarrayDeep512_512_512_AE20K.h5, and reference gene file, RefEntrezMicroarray.txt,  as input having the same order
    in which the autoencoder is trained, and prioritize the genes in the output file EntrezCorrectH1.txt for each node of the first hidden layer of the autoencoder
    in each column of the file.


(7) Lightup_FromSecondLayer.py prioritize the genes to inferring with the PPI module association from second hidden layer of the 3 layer deepAE, this code takes
    the trained autoencoder, e.g. MicroarrayDeep512_512_512_AE20K.h5, and the reference gene file, RefEntrezMicroarray.txt, as input and prioritize the genes
    in the output file EntrezCorrectH2.txt for each node of the second hidden layer of the autoencoder in each column of the file.

(8) Lightup_FromThirdLayer.py prioritize the genes to to inferring with the PPI module association from third hidden layer of the 3 layer deepAE, this code takes
    the trained autoencoder, e.g. MicroarrayDeep512_512_512_AE20K.h5, and the reference gene file, RefEntrezMicroarray.txt, as input and prioritize the genes
    in the output file EntrezCorrectH3.txt for each node of the third hidden layer of the autoencoder in each column of the file.
    
(9) BetCentVis.R and Layer_AverageDist.R computes  betweenness centrality and the average distance for the priotised the genes
    stored in the files EntrezCorrectH1.txt, EntrezCorrectH2.txt and EntrezCorrectH3.txt. It also takes PPI stored in Entrez_PPi.txt. The both codes depend
    on the igraph library. 
